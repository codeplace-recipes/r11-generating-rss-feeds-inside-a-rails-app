atom_feed do |feed|
  feed.title 'Codeplace Articles'
  feed.updated @articles.maximum(:updated_at)
  Article.all.each do |article|
    feed.entry article, published: article.published_on do |entry|
      entry.title article.name
      entry.content article.content
      entry.author do |author|
        author.name article.user.email
      end
    end
  end
end
