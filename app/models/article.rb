class Article < ApplicationRecord
  belongs_to :user
  validates_date :published_on, on_or_after: -> { Date.current }
  scope :published, -> { where('published_on <= ?', Time.now) }
end
