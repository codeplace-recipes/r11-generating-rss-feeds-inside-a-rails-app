# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user1 = User.new(email: 'user1@example.com', password: '123456')
user2 = User.new(email: 'user2@example.com', password: '123456')

Article.create(
  [
    {
      name: 'Imagine what you could build if you learned Ruby on Rails...',
      content: 'Learning to build a modern web application is daunting. Ruby on Rails makes it much easier and more fun. It includes everything you need to build fantastic applications, and you can learn it with the support of our large, friendly community.',
      published_on: Time.current,
      user: user1
    },
    {
      name: 'Learn by building - HTML5, CSS3, JavaScript / jQuery',
      content: 'Becoming a junior web developer is impossible without learning about basic blocks of every website out there. Sure all the back-end technologies are cool, but front-end basics are a must!',
      published_on: Time.current,
      user: user2
    },
    {
      name: 'Ember JS - A framework for creating ambitious web applications.',
      content: 'Ember.js is an open-source JavaScript web framework, based on the Model–view–viewmodel (MVVM) pattern. It allows developers to create scalable single-page web applications[1] by incorporating common idioms and best practices into the framework.',
      published_on: Time.current,
      user: user1
    },
    {
      name: 'Angular 2 - One framework. Mobile & desktop.',
      content: 'Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target. For web, mobile web, native mobile and native desktop.',
      published_on: Time.current,
      user: user2
    },
    {
      name: 'React - A JavaScript library for building user interfaces',
      content: 'React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.',
      published_on: Time.current,
      user: user2
    },
    {
      name: 'Ionic - The top open source framework for building amazing mobile apps.',
      content: 'Know how to build websites? Then you already know how to build mobile apps. Ionic Framework offers the best web and native app components for building highly interactive native and progressive web apps with Angular.',
      published_on: Time.current,
      user: user1
    },
    {
      name: 'Bootstrap 4 - the most popular HTML, CSS, and JS framework in the world.',
      content: "Bootstrap is made for building responsive, mobile-first projects and makes front-end web development faster and easier. It's made for folks of all skill levels, devices of all shapes, and projects of all sizes. Now in a new shape with flexbox, out of box!",
      published_on: Time.current,
      user: user2
    },
    {
      name: 'Node JS - Asynchronous event driven JavaScript runtime, designed to build scalable network applications.',
      content: "Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine. Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient. Node.js' package ecosystem, npm, is the largest ecosystem of open source libraries in the world",
      published_on: Time.current,
      user: user1
    },
    {
      name: 'Sinatra - DSL for quickly creating web applications in Ruby with minimal effort.',
      content: 'Sinatra is small and flexible. It does not follow the typical model–view–controller pattern used in other frameworks, such as Ruby on Rails. Instead, Sinatra focuses on "quickly creating web-applications in Ruby with minimal effort."',
      published_on: Time.current,
      user: user1
    },
    {
      name: 'Django - The web framework for perfectionists with deadlines.',
      content: 'Django is a high-level Python Web framework that encourages rapid development and clean, pragmatic design. Built by experienced developers, it takes care of much of the hassle of Web development, so you can focus on writing your app without needing to reinvent the wheel. It’s free and open source.',
      published_on: Time.current,
      user: user2
    }
  ]
)
